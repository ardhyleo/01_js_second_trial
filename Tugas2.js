// initiate from Human directly
class Human {
  static groupHealing = true;
  static group = "Vertebrate";
  constructor(name, address) {
    this.name = name;
    this.address = address;
  }
  introduce() {
    console.log(`Hi, my name is ${this.name}`);
  }
  work() {
    console.log("Work");
  }
}

class Programmer extends Human {
  constructor(name, address, programmingLanguages) {
    super(name, address);
    this.programmingLanguages = programmingLanguages;
  }
  introduce() {
    super.introduce();
    console.log(`I can write`, this.programmingLanguages);
  }

  code() {
    console.log("code some", this.programmingLanguages[Math.floor(Math.random() * this.programmingLanguages.length)]);
  }
}
let Obama = new Human("Barack Obama", "Washington Dc", "Javascript");
Obama.introduce();

let Isyana = new Programmer("Isyana Karina", "Jakarta", ["Javascript", "Ruby", "Go"]);
Isyana.introduce();
Isyana.code();
Isyana.work();

try {
  // Obama.code();
} catch (err) {
  console.log(err.message);
}

console.log(Isyana instanceof Human);
console.log(Isyana instanceof Programmer);
console.log(Human.groupHealing);
console.log(Human.group);
